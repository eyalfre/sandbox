// physical setup description:
// a water pump is connected to a water filter which cleans the water and then:
// 1) most of the water are returned back to the pond
// 2) a small amount of water are dumped outside to avoid overflow in the pond
// a float switch is constantly sensing the water level of the pond to determine when to start and stop the pump

#define PUMP_RELAY_PIN 2 // "s" to pin 2, "+" to VDD, "-" to GND

// the float_switch circuit is closed when water level is low and open when water level is high
#define FLOAT_SWITCH_PIN 12 // one leg to pin 12, one leg to GND

#define CONFIDENCE_THRESHOLD 10 // sensing the water level a few times to be sure the water is actually high (and not because of a wave)

#define FLOAT_LEVEL_LOW LOW
#define FLOAT_LEVEL_HIGH HIGH

int confidence = 0;
unsigned long working_time_ms = 1000UL * 60 * 1; // 1 min


void setup() {
	Serial.begin(9600);

	// configure pin as an OUTPUT / INPUT
	pinMode(PUMP_RELAY_PIN, OUTPUT);

	pinMode(FLOAT_SWITCH_PIN, INPUT_PULLUP); // using internal resistor

	// turn stuff off before starting
	digitalWrite(PUMP_RELAY_PIN, LOW);
}


bool is_water_level_high() {
	return digitalRead(FLOAT_SWITCH_PIN) == FLOAT_LEVEL_HIGH;
}

bool pump_is_working = false;
void activate_pump() {
	if (pump_is_working == false) {
		Serial.println("activating pump");
		digitalWrite(PUMP_RELAY_PIN, HIGH); // turn pump ON
		pump_is_working = true;
	}
}

void deactivate_pump() {
	if (pump_is_working == true) {
		Serial.println("deactivating pump");
		digitalWrite(PUMP_RELAY_PIN, LOW); // turn pump OFF
		pump_is_working = false;
	}
}

void loop() {
	bool is_high = is_water_level_high();
	Serial.print("water level = ");
	Serial.print(is_high ? "HIGH" : "LOW");
	Serial.print(", confidence = ");
	Serial.print(confidence);
	Serial.print("/");
	Serial.println(CONFIDENCE_THRESHOLD);

	if (is_high) {
		confidence++;
	} else { // in case the sensor was high because of a wave
		confidence = 0;
	}

	if (confidence > CONFIDENCE_THRESHOLD) {
		confidence = 0;

		activate_pump();

		while (is_water_level_high()) {
			Serial.print("activating pump for ");
			Serial.print((working_time_ms / 1000 / 60));
			Serial.println(" minutes");
			delay(working_time_ms);
		}

		deactivate_pump();
	}

	delay(1000);
}